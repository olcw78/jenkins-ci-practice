using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;

#if UNITY_EDITOR


using UnityEditor;
using UnityEditor.Build.Reporting;

public static class BuildConfigurations {
	static IEnumerable<string> FindScenesInBuildSettings =>
		EditorBuildSettings.scenes
			.Where((scene) => scene.enabled)
			.Select(scene => scene.path);
	
	[NotNull]
	static string StartedAt => DateTime.Now
		.ToLocalTime()
		.ToString(CultureInfo.CurrentCulture);

	[MenuItem("Build/Build Android Debug")]
	public static void BuildAndroid_Dbg() {
		if (EditorUserBuildSettings.activeBuildTarget != BuildTarget.Android) {
			EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
		}
		
		Debug.Log($"[Build Start] {StartedAt}");

		string apkPathWithApkName = Path.Combine(
			"Builds",
			"Android",
			$"Dbg_{PlayerSettings.Android.bundleVersionCode}.apk"
		);
		
		BuildReport buildReport = BuildPipeline.BuildPlayer(new BuildPlayerOptions {
			scenes = FindScenesInBuildSettings.ToArray(),
			locationPathName = apkPathWithApkName,
			target = BuildTarget.Android,
			options = BuildOptions.Development | BuildOptions.AllowDebugging,
		});

		LogBuildSummary(buildReport.summary);
	}

	[MenuItem("Build/Build IOS Debug")]
	public static void BuildIOS_Dbg() {
		if (EditorUserBuildSettings.activeBuildTarget != BuildTarget.iOS) {
			EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.iOS, BuildTarget.iOS);
		}

		Debug.Log($"[Build Start] {StartedAt}");

		string xcodeProjPath = Path.Combine("Builds", "iOS", "Dbg");
		BuildReport buildReport = BuildPipeline.BuildPlayer(new BuildPlayerOptions {
			scenes = FindScenesInBuildSettings.ToArray(),
			locationPathName = xcodeProjPath,
			target = BuildTarget.iOS,
			options = BuildOptions.Development | BuildOptions.AllowDebugging,
		});
		LogBuildSummary(buildReport.summary);
	}

	static void LogBuildSummary(BuildSummary buildSummary) {
		string endedAt = buildSummary.buildEndedAt.ToString(CultureInfo.CurrentCulture);

		switch (buildSummary.result) {
			case BuildResult.Succeeded: {
				Debug.Log($"[Build Success] Ended At : {endedAt}\n" +
				          $"Process Time: {buildSummary.buildStartedAt - buildSummary.buildEndedAt} seconds\n" +
				          $"Build Size: {buildSummary.totalSize} bytes\n"
				);
			}
				break;

			case BuildResult.Unknown:
			case BuildResult.Failed: {
				Debug.LogError($"[Build Failed] Ended At : {endedAt}" +
				               $"Process Time: {buildSummary.buildStartedAt - buildSummary.buildEndedAt} seconds\n"
				);
			}
				break;

			case BuildResult.Cancelled:
				break;

			default:
				throw new ArgumentOutOfRangeException();
		}
	}

	static void LogToFileBuildSummary(BuildSummary buildSummary) {
		//
	}
}
#endif
